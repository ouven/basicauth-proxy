package main

import (
	"log"
	"net/http"
	"os"
	"strings"

	"gitlab.com/ouven/basicauth-proxy/utilhtpasswd"
)

type envCryptBackendImpl struct {
	table *utilhtpasswd.PasswdTable
}

func envCryptBackend() basicAuthBackend {
	users := strings.TrimSpace(os.Getenv("PASSWD"))
	if len(users) == 0 {
		users = "test:$apr1$CEkIU63D$QTLTIJG3x5qRp7XI81htF1"
		log.Println(`PASSWD is empty. Assuming "test:test"`)
	}

	pwt, err := utilhtpasswd.NewFromBuffer([]byte(users))
	if err != nil {
		log.Panic(err)
	}

	return &envCryptBackendImpl{
		table: pwt,
	}
}

func (b *envCryptBackendImpl) validate(username, password string) bool {
	return b.table.Match(username, password)
}

func (b *envCryptBackendImpl) basicAuth(r *http.Request) bool {
	username, password, authOK := r.BasicAuth()
	return !authOK || !b.validate(username, password)
}
