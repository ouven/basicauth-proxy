# basicauth-proxy

## Backends

### env-crypt
...

### env-plain
Authorization comes from environment variable `USERS`. The variable must contain a comma separated list
of user passoword pairs in the form: `user:password`. Defaults to `test:test`

E.g. `a:b,b:c,test:test,foo:bar`

### k8s
see Kubenetes

### none
Just pass through


## Kubernetes

To run on kubernetes you can use a secret, with the contents of a htpasswd.

```yaml
    args:
    - -backend=k8s
    - -k8s-secret=basic-auth
    - -realm=authenticate
    - -proxy-pass=http://localhost:8080
```

You would need a secret
```yaml
---
apiVersion: v1
kind: Secret
metadata:
  name: basic-auth
type: Opaque
data:
  auth: dXNlcm5hbWU6PGFueSBjcnlwdCBmcm9tIGh0cGFzc3dkIGxpa2UgbWQ1LCBTaGEsIGJjcnlwdCwgc3NoYSwgcGxhaW4+
```

And RBAC roles and accounts
```yaml
---
apiVersion: v1
kind: ServiceAccount
metadata:
  namespace: default
  name: auth-proxy

---
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  namespace: default
  name: read-secrets
rules:
- apiGroups: [""]
  resources: ["secrets"]
  verbs: ["get", "watch"]

---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: read-secrets
  namespace: default
subjects:
- kind: ServiceAccount
  name: auth-proxy
roleRef:
  kind: Role
  name: read-secrets
  apiGroup: rbac.authorization.k8s.io
```
