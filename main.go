package main

import (
	"crypto/tls"
	"fmt"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"

	"github.com/namsral/flag"
)

type basicAuthBackend interface {
	basicAuth(*http.Request) bool
}

func main() {
	proxyPassFlag := flag.String("proxy-pass", "", "address to forward requests to")
	insecureFlag := flag.Bool("insecure", false, "verify tls of the proxy-pass")
	backendFlag := flag.String("backend", "env-plain", "select a backend to get password crypts from: env-plain, k8s")
	listenFlag := flag.String("listen", "0.0.0.0:80", "interface and port bind to listen at")
	realmFlag := flag.String("realm", "Restricted", "basic auth realm to challenge")

	flag.Parse()

	backendFactories := map[string]func() basicAuthBackend{
		"env-crypt": envCryptBackend,
		"env-plain": envPlainBackend,
		"k8s":       k8sBackendFactory,
		"none":      noneBackendFactory,
	}

	backendFactory, okBackend := backendFactories[*backendFlag]
	if !okBackend {
		panic(fmt.Sprintf("unknown backend: %s", *backendFlag))
	}
	backend := backendFactory()

	proxyURL, err := url.Parse(*proxyPassFlag)
	if err != nil {
		panic(err)
	}

	proxy := httputil.NewSingleHostReverseProxy(proxyURL)
	if *insecureFlag {
		proxy.Transport = &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
	}

	mux := http.NewServeMux()
	mux.HandleFunc("/ping", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("pong"))
	})
	mux.Handle("/", &basicAuthProxy{
		backend: backend,
		handler: proxy,
		realm:   *realmFlag,
	})

	log.Printf("listen to %s => %s", *listenFlag, proxyURL)
	log.Fatal(http.ListenAndServe(*listenFlag, mux))
}

type basicAuthProxy struct {
	backend basicAuthBackend
	handler http.Handler
	realm   string
}

func (ap *basicAuthProxy) challenge(w http.ResponseWriter) {
	w.Header().Set("WWW-Authenticate", `Basic realm="`+ap.realm+`"`)
	http.Error(w, "Not authorized", 401)
	return
}

func (ap *basicAuthProxy) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	if !ap.backend.basicAuth(r) {
		ap.challenge(w)
		return
	}

	ap.handler.ServeHTTP(w, r)
}
