package utilhtpasswd

import (
	"bufio"
	"bytes"
	"fmt"
	"strings"
)

type PasswdParser func(pw string) (EncodedPasswd, error)

type EncodedPasswd interface {
	MatchesPassword(pw string) bool
}

type PasswdTable map[string]EncodedPasswd

var parsers = []PasswdParser{AcceptMd5, AcceptSha, AcceptBcrypt, AcceptSsha, AcceptPlain}

func NewFromBuffer(b []byte) (*PasswdTable, error) {
	bf := &PasswdTable{}
	scanner := bufio.NewScanner(bytes.NewReader(b))
	for scanner.Scan() {
		line := scanner.Text()
		if perr := bf.addHtpasswdUser(line); perr != nil {
			return nil, perr
		}
	}
	if err := scanner.Err(); err != nil {
		return nil, fmt.Errorf("Error scanning htpasswd lines: %s", err.Error())
	}
	return bf, nil
}

func (bf *PasswdTable) Match(username, password string) bool {
	matcher, ok := (*bf)[username]
	return ok && matcher.MatchesPassword(password)
}

func (bf *PasswdTable) addHtpasswdUser(rawLine string) error {
	line := strings.TrimSpace(rawLine)
	if line == "" {
		return nil
	}

	// split "user:encoding" at colon
	parts := strings.SplitN(line, ":", 2)
	if len(parts) != 2 {
		return fmt.Errorf("malformed line, no colon: %s", line)
	}

	user, encoding := parts[0], parts[1]

	// give each parser a shot. The first one to produce a matcher wins.
	// If one produces an error then stop (to prevent Plain from catching it)
	for _, p := range parsers {
		matcher, err := p(encoding)
		if err != nil {
			return err
		}
		if matcher != nil {
			(*bf)[user] = matcher
			return nil // we are done, we took to first match
		}
	}

	// No one liked this line
	return fmt.Errorf("unable to recognize password for %s in %s", user, encoding)
}
