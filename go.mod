module gitlab.com/ouven/basicauth-proxy

go 1.14

require (
	github.com/namsral/flag v1.7.4-pre
	golang.org/x/crypto v0.0.0-20200728195943-123391ffb6de
)
