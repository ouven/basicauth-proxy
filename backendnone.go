package main

import (
	"net/http"

	"gitlab.com/ouven/basicauth-proxy/utilhtpasswd"
)

type noneBackend struct {
	table *utilhtpasswd.PasswdTable
}

func noneBackendFactory() basicAuthBackend {
	return &noneBackend{}
}

func (b *noneBackend) basicAuth(r *http.Request) bool {
	return true
}
