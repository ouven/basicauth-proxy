package main

import (
	"errors"
	"io/ioutil"
	"log"
	"net/http"

	"gitlab.com/ouven/basicauth-proxy/utilhtpasswd"
)

type k8sBackend struct {
	table *utilhtpasswd.PasswdTable
}

func k8sBackendFactory() basicAuthBackend {
	auth, err := ioutil.ReadFile("/etc/service/secrets/auth/auth")
	if err != nil {
		log.Panic(errors.New("secret not found"), err)
	}

	pwt, err := utilhtpasswd.NewFromBuffer(auth)
	if err != nil {
		log.Panic(err)
	}

	return &k8sBackend{
		table: pwt,
	}
}

func (b *k8sBackend) validate(username, password string) bool {
	return b.table.Match(username, password)
}

func (b *k8sBackend) basicAuth(r *http.Request) bool {
	username, password, authOK := r.BasicAuth()
	return !authOK || !b.validate(username, password)
}
