FROM golang as build
WORKDIR /build
COPY go.mod .
COPY go.sum .
ENV CGO_ENABLED=0
ENV GOARCH=386
ENV GOOS=linux
RUN go mod download
COPY . .
RUN go build

FROM alpine:latest as tls
RUN apk --no-cache add tzdata zip ca-certificates
WORKDIR /usr/share/zoneinfo
# -0 means no compression.  Needed because go's
# tz loader doesn't handle compressed data.
RUN zip -r -0 /zoneinfo.zip .

FROM scratch
# the timezone data:
ENV ZONEINFO /zoneinfo.zip
COPY --from=tls /zoneinfo.zip /
# the tls certificates:
COPY --from=tls /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=build /build/basicauth-proxy /

# verify ssl
ENV INSECURE=false
# format: test:test,test2:test2
ENV USERS=
# proxy endpoint
ENV PROXY_URL=

EXPOSE 80
ENTRYPOINT [ "/basicauth-proxy" ]
