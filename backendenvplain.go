package main

import (
	"log"
	"net/http"
	"os"
	"strings"
)

type envPlainBackendImpl struct {
	users map[string]string
}

func envPlainBackend() basicAuthBackend {
	users := strings.TrimSpace(os.Getenv("USERS"))
	if len(users) == 0 {
		users = "test:test"
		log.Println(`USERS is empty. Assuming "` + users + `"`)
	}

	userToPassword := make(map[string]string)
	for _, userAndPass := range strings.Split(users, ",") {
		parts := strings.Split(userAndPass, ":")

		userToPassword[parts[0]] = parts[1]
	}

	return &envPlainBackendImpl{users: userToPassword}
}

func (b *envPlainBackendImpl) validate(username, password string) bool {
	usersPass, ok := b.users[username]
	return ok && usersPass == password
}

func (b *envPlainBackendImpl) basicAuth(r *http.Request) bool {
	username, password, authOK := r.BasicAuth()
	return !authOK || !b.validate(username, password)
}
